#3088 PiHat Project
EEE3088 Lesson6 Assignment
We are designing a UPS. 
This UPS consists of a voltage regulator, OpAmp constrainer and LED statuses
This will ensure that when the power goes out then it will be powered by
batteries. It will operate with a voltage of 0-3.3V. The LEDs will indicate
when the battery needs to be activated as the power is off.

Bill of materials for circuit:
Transformer
Voltage sources
OpAmp
Resistors
Capacitors
Zener diodes
LEDs
Batteries
